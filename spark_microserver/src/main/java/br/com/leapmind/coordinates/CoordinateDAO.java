package br.com.leapmind.coordinates;

import lombok.Data;

@SuppressWarnings ("unused")
@Data
public class CoordinateDAO{
    private Integer id;
    private Double  lat;
    private Double  lng;
}
