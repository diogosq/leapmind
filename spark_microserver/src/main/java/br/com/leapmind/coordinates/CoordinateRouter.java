package br.com.leapmind.coordinates;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.leapmind.Model;
import spark.Route;

public class CoordinateRouter{

    private static final int HTTP_BAD_REQUEST = 400;

    private Model model;
    public final Route get = (request, response) -> {
        response.status(200);
        response.type("application/json");
        return Model.dataToJson(model.getAllPosts());
    };
    public final Route add = (request, response) -> {
        try{
            ObjectMapper mapper = new ObjectMapper();
            CoordinatePayload creation = mapper.readValue(request.body(), CoordinatePayload.class);
            if(!creation.isValid()){
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            int id = model.addCoordinate(creation.getLat(), creation.getLng());
            response.status(200);
            response.type("application/json");
            return id;
        }catch(JsonParseException jpe){
            response.status(HTTP_BAD_REQUEST);
            return "";
        }
    };
    public final Route getAll = (request, response) -> {
        response.status(200);
        return model.deleteAll();
    };

    public CoordinateRouter(Model model){
        this.model = model;
    }
}
