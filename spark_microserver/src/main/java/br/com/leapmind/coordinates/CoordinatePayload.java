package br.com.leapmind.coordinates;

import lombok.Data;

@SuppressWarnings ("unused")
@Data
public class CoordinatePayload{
    private Double lat;
    private Double lng;

    @SuppressWarnings ("BooleanMethodIsAlwaysInverted")
    public boolean isValid(){
        return lat != null && lng != null;
    }
}