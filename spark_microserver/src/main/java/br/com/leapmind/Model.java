package br.com.leapmind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.leapmind.coordinates.CoordinateDAO;

public class Model{
    private int                         nextId      = 1;
    private Map<Integer, CoordinateDAO> coordinates = new HashMap<>();

    public static String dataToJson(Object data){
        try{
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, data);
            return sw.toString();
        }catch(IOException e){
            throw new RuntimeException("IOException from a StringWriter?");
        }
    }

    @SuppressWarnings ("SameReturnValue")
    public String deleteAll(){

        coordinates = new HashMap<>();
        nextId = 1;
        return "OK";
    }

    public int addCoordinate(Double lat, Double lng){
        int id = nextId++;
        CoordinateDAO coordinate = new CoordinateDAO();
        coordinate.setId(id);
        coordinate.setLat(lat);
        coordinate.setLng(lng);
        coordinates.put(id, coordinate);
        return id;
    }

    public List getAllPosts(){
        return (List) coordinates.keySet().stream().sorted().map((id) -> coordinates.get(id)).collect(Collectors.toList());
    }
}