package br.com.leapmind;


import org.apache.log4j.Logger;

import br.com.leapmind.coordinates.CoordinateRouter;
import br.com.leapmind.utils.SparkUtils;


import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.Spark.post;


public class Application{

    private CoordinateRouter coordinateRouter;
    private Logger           logger;

    public static void main(String[] args){
        Application app = new Application();
        app.init();
    }

    private void init(){
        logger = Logger.getLogger(Application.class);

        port(8088);
        Model model = new Model();
        logger = Logger.getLogger("Application.class");
        coordinateRouter = new CoordinateRouter(model);

        SparkUtils.createServerWithRequestLog(logger);

        path("/api", () -> {
            before("/*", (q, a) -> logger.info("Received api call"));
            path("/coordinate", () -> {
                get("/all", coordinateRouter.get);
                post("/add", coordinateRouter.add);
                delete("/all", coordinateRouter.getAll);
            });

        });
    }


}