<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
        #map {
            height: 100%;
        }

        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script>
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: ${COORDINATE_CENTER},
            zoom: 17
        });
    ${MARKERS}

    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API}&callback=initMap"
        async defer></script>
</body>
</html>