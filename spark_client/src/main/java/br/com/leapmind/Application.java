package br.com.leapmind;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import br.com.leapmind.map.MapRouter;
import br.com.leapmind.utils.SparkUtils;


import static spark.Spark.get;
import static spark.Spark.port;

public class Application{

    public static void main(String[] args){
        Application app = new Application();
        app.init();
    }

    private void init(){
        port(4848);
        Logger logger = Logger.getLogger(Application.class);
        logger.setLevel(Level.INFO);

        MapRouter mapDao = new MapRouter(logger);
        SparkUtils.createServerWithRequestLog(logger);

        get("/map", mapDao.render);
    }


}