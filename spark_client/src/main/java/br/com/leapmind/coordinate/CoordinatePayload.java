package br.com.leapmind.coordinate;

import lombok.Data;

@SuppressWarnings ("unused")
@Data
public class CoordinatePayload{
    private Integer id;
    private Double  lat;
    private Double  lng;

    public boolean isValid(){
        return id != null && lat != null && lng != null;
    }
}