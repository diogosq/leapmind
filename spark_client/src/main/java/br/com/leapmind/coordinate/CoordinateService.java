package br.com.leapmind.coordinate;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.GET;

public interface CoordinateService{
    Retrofit retrofit =
            new Retrofit.Builder().baseUrl("http://localhost:8088/api/coordinate/").addConverterFactory(JacksonConverterFactory.create())
                                  .build();

    @GET ("all")
    Call<List<CoordinatePayload>> getAll();
}
