package br.com.leapmind.map;

import org.apache.log4j.Logger;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.leapmind.Application;
import br.com.leapmind.coordinate.CoordinatePayload;
import br.com.leapmind.coordinate.CoordinateService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import retrofit2.Call;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class MapRouter{


    private static final String GOOGLE_API = "AIzaSyBua6VDIfAT6tQ_WqpdozWvLaNTV5OSMGY";
    private final Configuration configuration = new Configuration(new Version(2, 3, 0));
    private Logger logger;
    public final Route render = (Request request, Response response) -> {
        response.status(200);
        response.type("text/html");

        StringWriter writer = new StringWriter();

        try{
            Template mapTemplate = configuration.getTemplate("templates/map.ftl");

            Map<String, Object> attributes = new HashMap<>();
            attributes.put("GOOGLE_API", GOOGLE_API);
            attributes.put("COORDINATE_CENTER", "{lat: -19.942455, lng: -43.947020}");

            CoordinateService coordinateService = CoordinateService.retrofit.create(CoordinateService.class);
            Call<List<CoordinatePayload>> call = coordinateService.getAll();
            List<CoordinatePayload> result = call.execute().body();

            String markerTemplate = "var marker%s = new google.maps.Marker({position:{lat: %s, lng: %s},map: map});";

            String markers = "";
            if(result != null){
                for(CoordinatePayload cp : result){
                    markers = markers.concat(
                            String.format(markerTemplate, cp.getId().toString(), cp.getLat().toString(), cp.getLng().toString()));
                }
            }

            logger.warn("asd " + markers);
            attributes.put("MARKERS", markers);

            mapTemplate.process(attributes, writer);
        }catch(Exception e){
            logger.error(e.getMessage(), e);

            //noinspection ThrowableResultOfMethodCallIgnored
            Spark.halt(500);
        }

        return writer;
    };


    public MapRouter(Logger logger){
        this.logger = logger;
        configuration.setClassForTemplateLoading(Application.class, "/");
    }
}
